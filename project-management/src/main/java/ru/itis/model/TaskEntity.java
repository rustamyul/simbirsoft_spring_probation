package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.itis.api.enums.TaskState;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true, exclude = {"author", "executor", "project"})
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "task")
public class TaskEntity extends AbstractEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private TaskState state;

    @ManyToOne
    @JoinColumn(name = "release_id", referencedColumnName = "id")
    private ReleaseEntity release;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private UserEntity author;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "executor_id", referencedColumnName = "id")
    private UserEntity executor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    private ProjectEntity project;
}
