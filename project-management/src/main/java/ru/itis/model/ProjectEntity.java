package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.itis.api.enums.ProjectState;

import javax.persistence.*;
import java.util.List;


@EqualsAndHashCode(callSuper = true, exclude = {"author", "tasks"})
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "project")
public class ProjectEntity extends AbstractEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private ProjectState state;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private UserEntity author;

    @OneToMany(mappedBy = "project")
    private List<TaskEntity> tasks;
}
