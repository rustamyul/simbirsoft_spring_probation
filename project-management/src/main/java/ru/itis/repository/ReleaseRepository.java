package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.itis.model.ReleaseEntity;

import java.util.UUID;

public interface ReleaseRepository extends JpaRepository<ReleaseEntity, UUID>, JpaSpecificationExecutor<ReleaseEntity> {
}
