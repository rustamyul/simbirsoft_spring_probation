package ru.itis.api.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

@Builder
@Schema(description = "информация об исключении")
public class ExceptionResponse {

    @JsonProperty("Сообщение")
    @Schema(description = "message", example = "User not found")
    private String message;

    @Schema(description = "Название исключения", example = "UserNotFoundException")
    @JsonProperty("exception_name")
    private String exceptionName;

}
