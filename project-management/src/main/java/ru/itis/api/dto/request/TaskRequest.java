package ru.itis.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Задача")
public class TaskRequest {

    @Schema(description = "Название задачи", example = "Регистрация")
    @JsonProperty("title")
    private String title;

    @Schema(description = "Описание задачи", example = "Реализовать...")
    @JsonProperty("description")
    private String description;

    @Schema(description = "Проект", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("project_id")
    private UUID projectId;

    @Schema(description = "Релиз", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("release_id")
    private UUID releaseId;

    @Schema(description = "исполнитель", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("executor_id")
    private UUID executorId;
}
