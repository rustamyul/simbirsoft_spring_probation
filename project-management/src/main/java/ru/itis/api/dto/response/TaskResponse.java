package ru.itis.api.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Задача")
public class TaskResponse {

    @Schema(description = "id задачи", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "Название задачи", example = "Регистрация")
    @JsonProperty("title")
    private String title;

    @Schema(description = "Описание задачи", example = "Реализовать...")
    @JsonProperty("description")
    private String description;

    @Schema(description = "Релиз")
    @JsonProperty("release")
    private ReleaseResponse release;
}
