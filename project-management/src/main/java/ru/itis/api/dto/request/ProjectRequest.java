package ru.itis.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Проект")
public class ProjectRequest {

    @Schema(description = "Название проекта", example = "Healthest")
    @JsonProperty("title")
    private String title;

    @Schema(description = "Описание проекта", example = "Платформа для объединения всех мед.учеждений в одну систему")
    @JsonProperty("description")
    private String description;
}
