package ru.itis.api.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;
import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Релиз")
public class ReleaseResponse {

    @Schema(description = "id релиза", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "Версия релиза", example = "3.1v")
    @JsonProperty("version")
    private String version;

    @Schema(description = "Время начало выполнения работ", example = "2023-12-17T13:00:00.100000000+03:00[Europe/Moscow]")
    @JsonProperty("start_date")
    private ZonedDateTime startDate;

    @Schema(description = "Время завершения работ", example = "2023-12-17T13:00:00.100000000+03:00[Europe/Moscow]")
    @JsonProperty("finish_date")
    private ZonedDateTime finishDate;
}
