package ru.itis.api.dto.request.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для обновления информации о пользователе")
public class UpdateUserRequest {

    @Schema(description = "имя пользователя", example = "Кирилл")
    @JsonProperty("name")
    private String name;

    @Schema(description = "Почта пользователя", example = "ivanov@mail.ru")
    @JsonProperty("email")
    private String email;
}
