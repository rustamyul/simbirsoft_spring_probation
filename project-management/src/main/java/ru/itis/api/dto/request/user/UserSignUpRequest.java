package ru.itis.api.dto.request.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.itis.api.enums.UserRole;
import ru.itis.api.validation.ValidSameFields;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для регистрации пользователя")
@ValidSameFields(names = {"password", "repeatPassword"}, message = "Passwords do not match")
public class UserSignUpRequest {

    @Schema(description = "имя пользователя", example = "Кирилл")
    @JsonProperty("name")
    @NotBlank(message = "field \"name\" cannot be empty")
    private String name;

    @Schema(description = "Почта пользователя", example = "ivanov@mail.ru")
    @JsonProperty("email")
    @NotBlank(message = "field \"email\" cannot be empty")
    @Email(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",
            message = "Email not valid")
    private String email;

    @Schema(description = "Новый пароль пользователя", example = "qwerty000")
    @JsonProperty("password")
    @NotBlank(message = "field \"password\" cannot be empty")
    private String password;

    @Schema(description = "Повтор нового пароля", example = "qwerty000")
    @JsonProperty("repeat_password")
    private String repeatPassword;

    @Schema(description = "Роль пользователя", example = "USER")
    @JsonProperty("role")
    private UserRole role;
}
