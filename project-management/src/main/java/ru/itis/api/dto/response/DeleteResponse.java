package ru.itis.api.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;

@Schema(description = "Результат удаления объекта")
public class DeleteResponse {

    @Schema(description = "Сообщение")
    @JsonProperty("message")
    private final String message = "Entity deleted successfully";

    @Schema(description = "Статус ответа")
    @JsonProperty("status")
    private final HttpStatus httpStatus = HttpStatus.OK;
}
