package ru.itis.api.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import ru.itis.api.enums.ProjectState;

import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Информация о проекте")
public class ProjectResponse {

    @Schema(description = "id проекта", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "Название проекта", example = "Healthest")
    @JsonProperty("title")
    private String title;

    @Schema(description = "Описание проекта", example = "Платформа для объединения всех мед.учеждений в одну систему")
    @JsonProperty("description")
    private String description;

    @Schema(description = "Статус проекта", example = "CREATE")
    @JsonProperty("state")
    private ProjectState state;
}
