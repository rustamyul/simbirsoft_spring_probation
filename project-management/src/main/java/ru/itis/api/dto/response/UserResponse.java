package ru.itis.api.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Форма для получения информации о пользователи")
public class UserResponse {

    @Schema(description = "id пользователя", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "имя пользователя", example = "Кирилл")
    @JsonProperty("name")
    private String name;

    @Schema(description = "Почта пользователя", example = "ivanov@mail.ru")
    @JsonProperty("email")
    private String email;
}
