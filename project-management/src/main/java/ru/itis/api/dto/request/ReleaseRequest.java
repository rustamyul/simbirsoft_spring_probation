package ru.itis.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Релиз")
public class ReleaseRequest {

    @Schema(description = "Версия релиза", example = "3.1v")
    @JsonProperty("version")
    private String version;

    @Schema(description = "Время начало выполнения работ", example = "2023-12-17T13:00:00.100000000+03:00[Europe/Moscow]")
    @JsonProperty("start_date")
    private ZonedDateTime startDate;

    @Schema(description = "Время завершения работ", example = "2023-12-17T13:00:00.100000000+03:00[Europe/Moscow]")
    @JsonProperty("finish_date")
    private ZonedDateTime finishDate;
}
