package ru.itis.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itis.api.dto.request.ProjectRequest;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.ProjectResponse;

import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/api/projects")
public interface ProjectApi<T, E> {

    @Operation(summary = "Получение проекта через id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Проект",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Project not found", content = @Content)
    })
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    ProjectResponse getProject(@PathVariable("id") UUID id);

    @Operation(summary = "Получение списка из проектов с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список проектов с пагинацией",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PageOfListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @GetMapping()
    @ResponseStatus(OK)
    PageOfListResponse<ProjectResponse> getProjects(@RequestParam("page") int page,
                                                    @RequestParam("size") int size,
                                                    Specification<E> spec);

    @Operation(summary = "Создание проекта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Проект",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping
    @ResponseStatus(CREATED)
    ProjectResponse createProject(@AuthenticationPrincipal T userDetails,
                                  @RequestBody ProjectRequest request);

    @Operation(summary = "Обновление проекта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Обновленный проект",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Project not found", content = @Content)
    })
    @PutMapping("/{id}")
    @ResponseStatus(OK)
    ProjectResponse updateProject(@AuthenticationPrincipal T userDetails,
                                  @PathVariable("id") UUID projectId,
                                  @RequestBody ProjectRequest request);

    @Operation(summary = "Старт проекта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Проект",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Project not found", content = @Content)
    })
    @PostMapping("/start/{id}")
    @ResponseStatus(OK)
    ProjectResponse startProject(@AuthenticationPrincipal T userDetails,
                                 @PathVariable("id") UUID id);

    @Operation(summary = "Завершение проекта")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Проект",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Project not found", content = @Content)
    })
    @PostMapping("/finish/{id}")
    @ResponseStatus(OK)
    ProjectResponse finishProject(@PathVariable("id") UUID id);
}
