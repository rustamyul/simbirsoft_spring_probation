package ru.itis.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itis.api.dto.request.TaskRequest;
import ru.itis.api.dto.response.DeleteResponse;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.TaskResponse;
import ru.itis.api.enums.TaskState;

import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/tasks")
public interface TaskApi<T, E> {

    @Operation(summary = "Получение задачи через id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Задача",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TaskResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Task not found", content = @Content)
    })
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    TaskResponse getTask(@PathVariable("id") UUID id);

    @Operation(summary = "Получение списка из задач с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список задач с пагинацией",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PageOfListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @GetMapping()
    @ResponseStatus(OK)
    PageOfListResponse<TaskResponse> getTasks(@RequestParam("page") int page,
                                              @RequestParam("size") int size,
                                              Specification<E> spec);

    @Operation(summary = "Создание задачи")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Задача",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TaskResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping
    @ResponseStatus(CREATED)
    TaskResponse createTask(@AuthenticationPrincipal T userDetails,
                            @RequestBody TaskRequest request);

    @Operation(summary = "Обновление задачи")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Обновленная задача",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TaskResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Task not found", content = @Content)
    })
    @PutMapping("/{id}")
    @ResponseStatus(OK)
    TaskResponse updateTask(@AuthenticationPrincipal T userDetails,
                            @PathVariable("id") UUID id,
                            @RequestBody TaskRequest request);

    @Operation(summary = "Удаление задачи")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Результат удаления задачи",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = DeleteResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Task not found", content = @Content)
    })
    @DeleteMapping("/{id}")
    @ResponseStatus(OK)
    DeleteResponse deleteTask(@AuthenticationPrincipal T userDetails,
                              @PathVariable("id") UUID id);

    @Operation(summary = "Смена статуса задачи")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Задача",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TaskResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Task not found", content = @Content)
    })
    @PutMapping("/state/{id}")
    @ResponseStatus(OK)
    TaskResponse changeTaskState(@AuthenticationPrincipal T userDetails,
                                 @PathVariable("id") UUID id,
                                 @RequestParam("state") TaskState state);
}