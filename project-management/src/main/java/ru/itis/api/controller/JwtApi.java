package ru.itis.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.itis.api.dto.response.TokenCoupleResponse;

import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/api/token")
public interface JwtApi {

    @Operation(summary = "Обновление refresh и access token")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Refresh и access token",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TokenCoupleResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PutMapping
    @ResponseStatus(OK)
    TokenCoupleResponse refreshTokenCouple(@RequestBody TokenCoupleResponse tokenCouple);
}
