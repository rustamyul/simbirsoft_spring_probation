package ru.itis.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;
import ru.itis.api.dto.request.ReleaseRequest;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.ReleaseResponse;

import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/api/release")
public interface ReleaseApi<E> {

    @Operation(summary = "Получение релиза через id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Релиз",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ReleaseResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Release not found", content = @Content)
    })
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    ReleaseResponse getRelease(@PathVariable("id") UUID id);

    @Operation(summary = "Получение списка из релизов с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список релизов с пагинацией",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PageOfListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @GetMapping()
    @ResponseStatus(OK)
    PageOfListResponse<ReleaseResponse> getReleases(@RequestParam("page") int page,
                                                    @RequestParam("size") int size,
                                                    Specification<E> spec);

    @Operation(summary = "Создание релиза")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Релиз",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ReleaseResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping
    @ResponseStatus(CREATED)
    ReleaseResponse createRelease(@RequestBody ReleaseRequest request);

    @Operation(summary = "Обновление релиза")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Обновленный релиз",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ReleaseResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Release not found", content = @Content)
    })
    @PutMapping("/{id}")
    @ResponseStatus(OK)
    ReleaseResponse updateRelease(@PathVariable("id") UUID id,
                                  @RequestBody ReleaseRequest request);
}
