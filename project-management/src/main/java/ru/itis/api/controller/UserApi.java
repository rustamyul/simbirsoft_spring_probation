package ru.itis.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.itis.api.dto.request.user.UpdateUserRequest;
import ru.itis.api.dto.request.user.UserLoginRequest;
import ru.itis.api.dto.request.user.UserSignUpRequest;
import ru.itis.api.dto.response.DeleteResponse;
import ru.itis.api.dto.response.TokenCoupleResponse;
import ru.itis.api.dto.response.UserResponse;

import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/api/users")
public interface UserApi<T> {

    @Operation(summary = "Получение пользователя через id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    UserResponse getUser(@PathVariable("id") UUID id);

    @Operation(summary = "Аутентификация пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Refresh и access token",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = TokenCoupleResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "401", description = "Invalid authentication", content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    @PostMapping("/login")
    @ResponseStatus(OK)
    TokenCoupleResponse login(@RequestBody UserLoginRequest request);

    @Operation(summary = "Регистрация пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Пользователь",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping("/signUp")
    @ResponseStatus(CREATED)
    TokenCoupleResponse signUp(@RequestBody UserSignUpRequest request);

    @Operation(summary = "Обновление информации о пользователи")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Обновленный пользователь",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    @PutMapping
    @ResponseStatus(OK)
    UserResponse updateUser(@AuthenticationPrincipal T principal,
                            @RequestBody UpdateUserRequest request);

    @Operation(summary = "Удаление пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Результат удаления пользователя",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = DeleteResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)})
    @DeleteMapping
    @ResponseStatus(OK)
    DeleteResponse deleteUser(@AuthenticationPrincipal T principal);
}
