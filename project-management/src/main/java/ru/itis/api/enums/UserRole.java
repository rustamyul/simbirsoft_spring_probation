package ru.itis.api.enums;

public enum UserRole {
    PROJECT_CREATOR,
    TASK_CREATOR,
    TASK_EXECUTOR
}
