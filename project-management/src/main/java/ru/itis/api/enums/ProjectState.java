package ru.itis.api.enums;

public enum ProjectState {
    CREATE,
    IN_PROGRESS,
    DONE
}
