package ru.itis.api.enums;

public enum UserState {
    ALLOWED,
    BANNED,
    DELETED
}
