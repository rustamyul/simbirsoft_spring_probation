package ru.itis.api.enums;

public enum TaskState {
    BACKLOG,
    IN_PROGRESS,
    DONE
}
