package ru.itis.api.validation;

import ru.itis.api.validation.impl.SameFieldsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = SameFieldsValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidSameFields {

    String message() default "Fields are not equal";

    String[] names() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
