package ru.itis.api.validation.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Ошибки при валидации")
public class ValidationErrorResponse {

    @Schema(description = "Поле", example = "title")
    private String field;

    @Schema(description = "Объект", example = "user")
    private String object;

    @Schema(description = "Сообщение об ошибке", example = "Password is incorrect")
    private String message;
}
