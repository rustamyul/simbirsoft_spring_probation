package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.controller.ReleaseApi;
import ru.itis.api.dto.request.ReleaseRequest;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.ReleaseResponse;
import ru.itis.model.ReleaseEntity;
import ru.itis.service.ReleaseService;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
public class ReleaseController implements ReleaseApi<ReleaseEntity> {

    private final ReleaseService releaseService;

    @Override
    public ReleaseResponse getRelease(UUID id) {
        return releaseService.getRelease(id);
    }

    @Override
    public PageOfListResponse<ReleaseResponse> getReleases(int page, int size,
                                                           @Spec(path = "version", params = "version", spec = Like.class)
                                                                   Specification<ReleaseEntity> spec) {
        return releaseService.getReleases(page, size, spec);
    }

    @PreAuthorize("!hasAuthority('TASK_EXECUTOR')")
    @Override
    public ReleaseResponse createRelease(ReleaseRequest request) {
        return releaseService.createRelease(request);
    }

    @PreAuthorize("!hasAuthority('TASK_EXECUTOR')")
    @Override
    public ReleaseResponse updateRelease(UUID id, ReleaseRequest request) {
        return releaseService.updateRelease(id, request);
    }
}
