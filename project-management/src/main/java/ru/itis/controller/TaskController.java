package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.controller.TaskApi;
import ru.itis.api.dto.request.TaskRequest;
import ru.itis.api.dto.response.DeleteResponse;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.TaskResponse;
import ru.itis.api.enums.TaskState;
import ru.itis.model.TaskEntity;
import ru.itis.service.TaskService;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
public class TaskController implements TaskApi<UserDetails, TaskEntity> {

    private final TaskService taskService;

    @Override
    public TaskResponse getTask(UUID id) {
        return taskService.getTask(id);
    }

    @Override
    public PageOfListResponse<TaskResponse> getTasks(int page, int size,
                                                     @Join(path = "author", alias = "a")
                                                     @Join(path = "executor", alias = "e")
                                                     @Join(path = "project", alias = "p")
                                                     @Join(path = "release", alias = "r")
                                                     @And({
                                                             @Spec(path = "title", params = "title", spec = Like.class),
                                                             @Spec(path = "a.email", params = "author_email", spec = Like.class),
                                                             @Spec(path = "e.email", params = "executor_email", spec = Like.class),
                                                             @Spec(path = "p.title", params = "project_title", spec = Like.class),
                                                             @Spec(path = "r.version", params = "release_version", spec = Like.class)
                                                     }) Specification<TaskEntity> spec) {
        return taskService.getTasks(page, size, spec);
    }

    @PreAuthorize("!hasAuthority('TASK_EXECUTOR')")
    @Override
    public TaskResponse createTask(UserDetails userDetails, TaskRequest request) {
        return taskService.createTask(userDetails, request);
    }

    @PreAuthorize("!hasAuthority('TASK_EXECUTOR')")
    @Override
    public TaskResponse updateTask(UserDetails userDetails, UUID id, TaskRequest request) {
        return taskService.updateTask(userDetails, id, request);
    }

    @PreAuthorize("!hasAuthority('TASK_EXECUTOR')")
    @Override
    public DeleteResponse deleteTask(UserDetails userDetails, UUID id) {
        return taskService.deleteTask(userDetails, id);
    }

    @Override
    public TaskResponse changeTaskState(UserDetails userDetails, UUID id, TaskState state) {
        return taskService.changeTaskState(userDetails, id, state);
    }
}
