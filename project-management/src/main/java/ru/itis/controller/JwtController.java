package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.controller.JwtApi;
import ru.itis.api.dto.response.TokenCoupleResponse;
import ru.itis.jwt.JwtTokenService;

@RestController
@RequiredArgsConstructor
public class JwtController implements JwtApi {

    private final JwtTokenService jwtTokenService;

    @Override
    public TokenCoupleResponse refreshTokenCouple(TokenCoupleResponse tokenCouple) {
        return jwtTokenService.refreshAccessToken(tokenCouple);
    }
}
