package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.controller.UserApi;
import ru.itis.api.dto.request.user.UpdateUserRequest;
import ru.itis.api.dto.request.user.UserLoginRequest;
import ru.itis.api.dto.request.user.UserSignUpRequest;
import ru.itis.api.dto.response.DeleteResponse;
import ru.itis.api.dto.response.TokenCoupleResponse;
import ru.itis.api.dto.response.UserResponse;
import ru.itis.jwt.JwtTokenService;
import ru.itis.service.UserService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi<UserDetails> {

    private final UserService userService;
    private final JwtTokenService jwtTokenService;

    @Override
    public UserResponse getUser(UUID id) {
        return userService.getUser(id);
    }

    @Override
    public TokenCoupleResponse login(@Valid UserLoginRequest request) {
        return jwtTokenService.generateTokenCouple(userService.login(request));
    }

    @Override
    public TokenCoupleResponse signUp(@Valid UserSignUpRequest request) {
        return jwtTokenService.generateTokenCouple(userService.signUp(request));
    }

    @Override
    public UserResponse updateUser(UserDetails principal, UpdateUserRequest request) {
        return userService.updateUser(principal, request);
    }

    @Override
    public DeleteResponse deleteUser(UserDetails principal) {
        return userService.deleteUser(principal);
    }
}
