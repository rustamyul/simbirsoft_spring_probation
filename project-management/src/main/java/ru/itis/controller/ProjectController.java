package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Like;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.controller.ProjectApi;
import ru.itis.api.dto.request.ProjectRequest;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.ProjectResponse;
import ru.itis.model.ProjectEntity;
import ru.itis.service.ProjectService;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
public class ProjectController implements ProjectApi<UserDetails, ProjectEntity> {

    private final ProjectService projectService;

    @Override
    public ProjectResponse getProject(UUID id) {
        return projectService.getProject(id);
    }

    @Override
    public PageOfListResponse<ProjectResponse> getProjects(int page, int size,
                                                           @Spec(path = "title", params = "title", spec = Like.class)
                                                                   Specification<ProjectEntity> spec) {
        return projectService.getProjects(page, size, spec);
    }

    @PreAuthorize("hasAuthority('PROJECT_CREATOR')")
    @Override
    public ProjectResponse createProject(UserDetails userDetails, ProjectRequest request) {
        return projectService.createProject(userDetails, request);
    }

    @PreAuthorize("hasAuthority('PROJECT_CREATOR')")
    @Override
    public ProjectResponse updateProject(UserDetails userDetails, UUID projectId, ProjectRequest request) {
        return projectService.updateProject(userDetails, projectId, request);
    }

    @PreAuthorize("hasAuthority('PROJECT_CREATOR')")
    @Override
    public ProjectResponse startProject(UserDetails userDetails, UUID id) {
        return projectService.startProject(userDetails, id);
    }

    @PreAuthorize("hasAuthority('PROJECT_CREATOR')")
    @Override
    public ProjectResponse finishProject(UUID id) {
        return projectService.finishProject(id);
    }
}
