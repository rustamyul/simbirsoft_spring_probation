package ru.itis.utils.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.itis.api.dto.request.user.UpdateUserRequest;
import ru.itis.api.dto.response.UserResponse;
import ru.itis.model.UserEntity;

import static org.mapstruct.NullValueCheckStrategy.ALWAYS;
import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserResponse toUserResponse(UserEntity user);

    @BeanMapping(nullValuePropertyMappingStrategy = IGNORE,
            nullValueCheckStrategy = ALWAYS)
    void updateUserInfo(@MappingTarget UserEntity user, UpdateUserRequest request);
}
