package ru.itis.utils.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.itis.api.dto.request.ProjectRequest;
import ru.itis.api.dto.response.ProjectResponse;
import ru.itis.model.ProjectEntity;

import java.util.Collection;
import java.util.List;

import static org.mapstruct.NullValueCheckStrategy.ALWAYS;
import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;

@Mapper(componentModel = "spring")
public interface ProjectMapper {

    ProjectEntity toProjectEntity(ProjectRequest request);

    ProjectResponse toProjectResponse(ProjectEntity save);

    @BeanMapping(nullValuePropertyMappingStrategy = IGNORE,
            nullValueCheckStrategy = ALWAYS)
    void updateProject(@MappingTarget ProjectEntity project, ProjectRequest request);

    Collection<ProjectResponse> toProjectResponseList(List<ProjectEntity> result);
}
