package ru.itis.utils.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.itis.api.dto.request.TaskRequest;
import ru.itis.api.dto.response.TaskResponse;
import ru.itis.model.TaskEntity;

import java.util.Collection;
import java.util.List;

import static org.mapstruct.NullValueCheckStrategy.ALWAYS;
import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;

@Mapper(componentModel = "spring", uses = {ReleaseMapper.class})
public interface TaskMapper {

    TaskResponse toTaskResponse(TaskEntity taskEntity);

    TaskEntity toTaskEntity(TaskRequest request);

    @BeanMapping(nullValuePropertyMappingStrategy = IGNORE,
            nullValueCheckStrategy = ALWAYS)
    void updateTask(@MappingTarget TaskEntity task, TaskRequest request);

    Collection<TaskResponse> toTaskResponseList(List<TaskEntity> content);
}
