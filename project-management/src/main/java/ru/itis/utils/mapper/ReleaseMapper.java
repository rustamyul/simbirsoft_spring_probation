package ru.itis.utils.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.itis.api.dto.request.ReleaseRequest;
import ru.itis.api.dto.response.ReleaseResponse;
import ru.itis.model.ReleaseEntity;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;

import static org.mapstruct.NullValueCheckStrategy.ALWAYS;
import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;

@Mapper(componentModel = "spring")
public interface ReleaseMapper {

    ReleaseResponse toReleaseResponse(ReleaseEntity orElseThrow);

    ReleaseEntity toReleaseEntity(ReleaseRequest request);

    @BeanMapping(nullValuePropertyMappingStrategy = IGNORE,
            nullValueCheckStrategy = ALWAYS)
    void updateRelease(@MappingTarget ReleaseEntity release, ReleaseRequest request);

    default Instant mapInstant(ZonedDateTime time) {
        return Instant.from(time);
    }

    default ZonedDateTime mapZonedDateTime(Instant instant) {
        return instant.atZone(ZoneId.of("Europe/Moscow"));
    }

    Collection<ReleaseResponse> toReleaseResponseList(List<ReleaseEntity> content);
}
