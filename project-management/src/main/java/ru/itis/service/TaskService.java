package ru.itis.service;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.api.dto.request.TaskRequest;
import ru.itis.api.dto.response.DeleteResponse;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.TaskResponse;
import ru.itis.api.enums.TaskState;
import ru.itis.model.TaskEntity;

import java.util.UUID;

/**
 * Сервис для работы с задачей
 */
public interface TaskService {

    /**
     * Метод, предназначенный для получения задачи, используя его id.
     *
     * @param id - Id задачи.
     * @return Данные о задачи
     */
    TaskResponse getTask(UUID id);

    /**
     * Метод, предназначенный для получения списка из задач с пагинацией.
     *
     * @param page - Номер страницы.
     * @param size - Размер страницы.
     * @param spec - Спецификация для фильтрации задач.
     * @return Список из задач с пагинацией.
     */
    PageOfListResponse<TaskResponse> getTasks(int page, int size, Specification<TaskEntity> spec);

    /**
     * Метод, предназваченный для создания задачи.
     * Пользователь только с особой ролью может создать задачу.
     *
     * @param userDetails - Необходим для получения username пользователя, отправляющего запрос,
     *                    для дальнейшей привязки к задаче, в роли автора.
     * @param request     - Содержит информацию о задаче. В том числе исполнитель, проект, релиз.
     * @return Данные о задаче.
     */
    TaskResponse createTask(UserDetails userDetails, TaskRequest request);

    /**
     * Метод, предназваченный для обновления задачи.
     * Пользователь только с особой ролью может обновить задачу.
     *
     * @param userDetails - Необходим для получения username пользователя, отправляющего запрос,
     *                    для проверки на авторство.
     * @param id          - Id задачи.
     * @param request     - Содержит в себе обновленные данные о задаче.
     * @return Обновленные данные о задаче.
     */
    TaskResponse updateTask(UserDetails userDetails, UUID id, TaskRequest request);

    /**
     * Метод, предназваченный для удаления задачи.
     * Пользователь только с особой ролью может удалить задачу.
     *
     * @param userDetails - Необходим для получения username пользователя, отправляющего запрос,
     *                    для проверки на авторство.
     * @param id          - Id задачи.
     * @return Обновленные данные о задачи.
     */
    DeleteResponse deleteTask(UserDetails userDetails, UUID id);

    /**
     * Метод, предназваченный для изменения статуса задачи.
     *
     * @param userDetails - Необходим для получения username пользователя, отправляющего запрос,
     *                    для проверки прав на доступ к задаче.
     * @param id          - Id задачи.
     * @param state       - Новый статус задачи.
     * @return - Данные о задачи
     */
    TaskResponse changeTaskState(UserDetails userDetails, UUID id, TaskState state);
}
