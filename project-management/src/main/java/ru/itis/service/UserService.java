package ru.itis.service;

import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.api.dto.request.user.UpdateUserRequest;
import ru.itis.api.dto.request.user.UserLoginRequest;
import ru.itis.api.dto.request.user.UserSignUpRequest;
import ru.itis.api.dto.response.DeleteResponse;
import ru.itis.api.dto.response.UserResponse;
import ru.itis.exception.ApplicationServiceException;
import ru.itis.model.UserEntity;

import java.util.UUID;

/**
 * Сервис для работы с пользователем
 */
public interface UserService {

    /**
     * Метод, предназначенный для получения сущности UserEntity,
     * используя username из access token.
     *
     * @param subjectFromAccessToken - Username из access token.
     * @return сущность UserEntity.
     */
    UserEntity getUserBySubject(String subjectFromAccessToken);

    /**
     * Метод, предназначенный для авторизации пользователя.
     *
     * @param request - Форма с email и паролем.
     * @return сущность UserEntity.
     */
    UserEntity login(UserLoginRequest request);

    /**
     * Метод, предназначенный для регистрации пользователя.
     *
     * @param request - Форма для регистрации.
     * @return сущность UserEntity.
     */
    UserEntity signUp(UserSignUpRequest request);

    /**
     * Метод, предназначенный для получения пользователя, используя его id.
     *
     * @param id - Id пользователя
     * @return Данные о пользователе.
     */
    UserResponse getUser(UUID id);

    /**
     * Метод, предназначенный для обновления информации о пользователе.
     *
     * @param userDetails - Необходим для получения username пользователя, отправляющего запрос,
     *                    для обновления информации о себе.
     * @param request     - Содержит в себе обновленные данные о пользователе.
     * @return Обновленные данные о пользователе.
     */
    UserResponse updateUser(UserDetails userDetails, UpdateUserRequest request);

    /**
     * Метод, предназначенный для удаления аккаунта пользователя.
     *
     * @param userDetails - Необходим для получения username пользователя, отправляющего запрос,
     *                    для удаления аккаунта.
     * @return Уведомление об удалении.
     */
    DeleteResponse deleteUser(UserDetails userDetails);

    /**
     * Метод, предназначенный для проверки существования пользователя с таким email.
     *
     * @param email - Email
     * @throws ApplicationServiceException - если пользователь с таким email уже зарегестрирован в системе.
     */
    void checkExistUser(String email);
}
