package ru.itis.service;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.api.dto.request.ProjectRequest;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.ProjectResponse;
import ru.itis.model.ProjectEntity;

import java.util.UUID;

/**
 * Сервис для работы с <strong>проектом</strong>
 */
public interface ProjectService {

    /**
     * Метод, предназначенный для получения проекта, используя его id.
     *
     * @param id - Id проекта.
     * @return Данные о проекте
     */
    ProjectResponse getProject(UUID id);

    /**
     * Метод, предназначенный для получения списка из проектов с пагинацией
     *
     * @param page - Номер страницы
     * @param size - Размер страницы
     * @param spec - Спецификация для фильтрации проектов
     * @return Список из проектов с пагинацией
     */
    PageOfListResponse<ProjectResponse> getProjects(int page, int size, Specification<ProjectEntity> spec);

    /**
     * Метод, предназначенный для создания проекта.
     * Пользователь только с особой ролью может создать проект.
     *
     * @param userDetails - Необходим для получения username пользователя, отправляющего запрос,
     *                    для дальнейшей привязки к проекту.
     * @param request     - Содержит в себе данные о проекте
     * @return Данные о проекте
     */
    ProjectResponse createProject(UserDetails userDetails, ProjectRequest request);

    /**
     * Метод, предназначенный для обновления информации о проекте.
     * Пользователь только с особой ролью может обновить проект.
     *
     * @param userDetails - Необходим для получения username пользователя, отправляющего запрос,
     *                    для проверки на авторство.
     * @param id          - Id проекта, который нужно обновить.
     * @param request     - Содержит в себе обновленные данные о проекте
     * @return Обновленные данные о проекте
     */
    ProjectResponse updateProject(UserDetails userDetails, UUID id, ProjectRequest request);

    /**
     * Метод, предназначенный для запуска проекта.
     * Пользователь только с особой ролью может запустить проект.
     *
     * @param userDetails - Необходим для получения username пользователя, отправляющего запрос,
     *                    для проверки на авторство.
     * @param id          - Id проекта.
     * @return Данные о проекте
     */
    ProjectResponse startProject(UserDetails userDetails, UUID id);

    /**
     * Метод, предназначенный для завершения проекта.
     *
     * @param id - Id проекта.
     * @return Данные о проекте
     */
    ProjectResponse finishProject(UUID id);

}
