package ru.itis.service;

import org.springframework.data.jpa.domain.Specification;
import ru.itis.api.dto.request.ReleaseRequest;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.ReleaseResponse;
import ru.itis.model.ReleaseEntity;

import java.util.UUID;

/**
 * Сервис для работы с <strong>релизом</strong>
 */
public interface ReleaseService {

    /**
     * Метод, предназначенный для получения релиза, используя его id.
     *
     * @param id - Id релиза
     * @return Данные о релизе
     */
    ReleaseResponse getRelease(UUID id);

    /**
     * Метод, предназначенный для получения списка из релзов с пагинацией.
     *
     * @param page - Номер страницы.
     * @param size - Размер страницы.
     * @param spec - Спецификация для фильтрации релизов.
     * @return Список из релизов с пагинацией
     */
    PageOfListResponse<ReleaseResponse> getReleases(int page, int size, Specification<ReleaseEntity> spec);

    /**
     * Метод, предназначенный для создания релиза.
     * Пользователь только с особой ролью может создавать релиз.
     *
     * @param request - Содержит в себе данные о релизе.
     * @return Данные о релизе
     */
    ReleaseResponse createRelease(ReleaseRequest request);

    /**
     * Метод, предназначенный для обновления информации о релизе.
     * Пользователь только с особой ролью может обновлять релиз.
     *
     * @param id      - Id релиза
     * @param request - Содержит в себе обновленные данные о релизе.
     * @return Обновленные данные о релизе
     */
    ReleaseResponse updateRelease(UUID id, ReleaseRequest request);
}
