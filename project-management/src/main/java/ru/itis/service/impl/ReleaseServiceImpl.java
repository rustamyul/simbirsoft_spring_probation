package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.itis.api.dto.request.ReleaseRequest;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.ReleaseResponse;
import ru.itis.exception.ApplicationNotFoundException;
import ru.itis.model.ReleaseEntity;
import ru.itis.repository.ReleaseRepository;
import ru.itis.service.ReleaseService;
import ru.itis.utils.mapper.ReleaseMapper;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
public class ReleaseServiceImpl implements ReleaseService {

    private final ReleaseRepository releaseRepository;
    private final ReleaseMapper releaseMapper;

    @Override
    public ReleaseResponse getRelease(UUID id) {

        log.info("Get release through id, release-id - {}", id);
        return releaseMapper.toReleaseResponse(
                releaseRepository.findById(id)
                        .orElseThrow(() -> new ApplicationNotFoundException("Release not found"))
        );
    }

    @Override
    public PageOfListResponse<ReleaseResponse> getReleases(int page, int size, Specification<ReleaseEntity> spec) {

        log.info("Get list of releases with pagination, page - {}, size - {}", page, size);
        Page<ReleaseEntity> result = releaseRepository.findAll(spec, PageRequest.of(page, size, Sort.by("startDate").descending()));
        return PageOfListResponse.<ReleaseResponse>builder()
                .list((List<ReleaseResponse>) releaseMapper.toReleaseResponseList(result.getContent()))
                .size(size)
                .totalPage(result.getTotalPages())
                .build();
    }

    @Override
    public ReleaseResponse createRelease(ReleaseRequest request) {

        ReleaseEntity newRelease = releaseMapper.toReleaseEntity(request);
        releaseRepository.save(newRelease);
        log.info("Create release, release-id - {}", newRelease.getId());
        return releaseMapper.toReleaseResponse(newRelease);
    }

    @Transactional
    @Override
    public ReleaseResponse updateRelease(UUID id, ReleaseRequest request) {

        log.info("Update release, release-id - {}", id);
        ReleaseEntity release = releaseRepository.findById(id)
                .orElseThrow(() -> new ApplicationNotFoundException("Release not found"));
        releaseMapper.updateRelease(release, request);
        return releaseMapper.toReleaseResponse(release);
    }
}
