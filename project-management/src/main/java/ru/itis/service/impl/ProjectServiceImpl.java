package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.itis.api.dto.request.ProjectRequest;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.ProjectResponse;
import ru.itis.api.enums.ProjectState;
import ru.itis.api.enums.TaskState;
import ru.itis.exception.ApplicationNotFoundException;
import ru.itis.exception.ApplicationServiceException;
import ru.itis.model.ProjectEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.ProjectRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.ProjectService;
import ru.itis.utils.mapper.ProjectMapper;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;

    private final ProjectMapper projectMapper;

    @Override
    public ProjectResponse getProject(UUID id) {

        log.info("Get project through id, project-id - {}", id);
        return projectMapper.toProjectResponse(
                projectRepository.findById(id)
                        .orElseThrow(() -> new ApplicationNotFoundException("Project not found"))
        );
    }

    @Override
    public PageOfListResponse<ProjectResponse> getProjects(int page, int size,
                                                           Specification<ProjectEntity> spec) {
        log.info("Get list of projects with pagination, page - {}, size - {}", page, size);
        Page<ProjectEntity> result = projectRepository.findAll(spec, PageRequest.of(page, size, Sort.by("createDate").descending()));
        return PageOfListResponse.<ProjectResponse>builder()
                .list((List<ProjectResponse>) projectMapper.toProjectResponseList(result.getContent()))
                .size(size)
                .totalPage(result.getTotalPages())
                .build();
    }

    @Transactional
    @Override
    public ProjectResponse createProject(UserDetails userDetails, ProjectRequest request) {

        log.info("User with email - {} create project, project-title - {}", userDetails.getUsername(), request.getTitle());
        UserEntity author = userRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(() -> new ApplicationNotFoundException("User not found"));

        ProjectEntity newProject = projectMapper.toProjectEntity(request);
        newProject.setAuthor(author);
        newProject.setState(ProjectState.CREATE);

        return projectMapper.toProjectResponse(
                projectRepository.save(newProject));
    }

    @Transactional
    @Override
    public ProjectResponse updateProject(UserDetails userDetails, UUID id, ProjectRequest request) {

        log.info("User with email - {} update project, project-id - {}", userDetails.getUsername(), id);
        UserEntity author = userRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(() -> new ApplicationNotFoundException("User not found"));

        ProjectEntity project = projectRepository.findByIdAndAuthor(id, author)
                .orElseThrow(() -> new ApplicationNotFoundException("Project not found"));
        projectMapper.updateProject(project, request);

        return projectMapper.toProjectResponse(project);
    }

    @Transactional
    @Override
    public ProjectResponse startProject(UserDetails userDetails, UUID id) {

        log.info("User with email - {} start project, project-id - {}", userDetails.getUsername(), id);
        ProjectEntity project = projectRepository.findById(id)
                .orElseThrow(() -> new ApplicationNotFoundException("Project not found"));

        if (!project.getAuthor().getEmail().equals(userDetails.getUsername())) {
            throw new ApplicationServiceException(HttpStatus.FORBIDDEN, "Access to the project is denied to the user");
        }
        project.setState(ProjectState.IN_PROGRESS);
        return projectMapper.toProjectResponse(project);
    }

    @Transactional
    @Override
    public ProjectResponse finishProject(UUID id) {

        log.info("Finish project through id, project-id - {}", id);
        ProjectEntity project = projectRepository.findById(id)
                .orElseThrow(() -> new ApplicationNotFoundException("Project not found"));

        long countNotDoneTask = project.getTasks().stream()
                .filter(task -> !task.getState().equals(TaskState.DONE))
                .count();

        if (countNotDoneTask == 0) {
            project.setState(ProjectState.DONE);
        } else {
            throw new ApplicationServiceException(HttpStatus.BAD_REQUEST, "Not all tasks have status of \"DONE\"");
        }
        return projectMapper.toProjectResponse(project);
    }
}
