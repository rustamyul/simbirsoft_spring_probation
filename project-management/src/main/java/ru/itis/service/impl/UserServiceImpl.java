package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.api.dto.request.user.UpdateUserRequest;
import ru.itis.api.dto.request.user.UserLoginRequest;
import ru.itis.api.dto.request.user.UserSignUpRequest;
import ru.itis.api.dto.response.DeleteResponse;
import ru.itis.api.dto.response.UserResponse;
import ru.itis.api.enums.UserState;
import ru.itis.exception.ApplicationNotFoundException;
import ru.itis.exception.ApplicationServiceException;
import ru.itis.model.UserEntity;
import ru.itis.repository.UserRepository;
import ru.itis.service.UserService;
import ru.itis.utils.mapper.UserMapper;

import javax.transaction.Transactional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;


    @Override
    public UserEntity getUserBySubject(String email) {

        log.info("Get user information through user-email, user-email - {}", email);
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new ApplicationNotFoundException("User not found"));
    }

    @Override
    public UserResponse getUser(UUID id) {

        log.info("Get user through id, user-id - {}", id);
        return userMapper.toUserResponse(userRepository.findById(id)
                .orElseThrow(() -> new ApplicationNotFoundException("User not found")));
    }

    @Override
    public UserEntity login(UserLoginRequest request) {

        log.info("Authentication user, user-email - {}", request.getEmail());
        UserEntity user = getUserBySubject(request.getEmail());

        if (!passwordEncoder.matches(request.getPassword(), user.getHashPassword()))
            throw new ApplicationNotFoundException("User not found");

        return user;
    }

    @Transactional
    @Override
    public UserEntity signUp(UserSignUpRequest request) {

        log.info("User registration, user-email - {}", request.getEmail());
        checkExistUser(request.getEmail());

        return userRepository.save(
                UserEntity.builder()
                        .name(request.getName())
                        .email(request.getEmail())
                        .hashPassword(passwordEncoder.encode(request.getPassword()))
                        .role(request.getRole())
                        .state(UserState.ALLOWED)
                        .build());
    }

    @Transactional
    @Override
    public UserResponse updateUser(UserDetails userDetails, UpdateUserRequest request) {

        log.info("Update user, user-email - {}", userDetails.getUsername());
        checkExistUser(request.getEmail());
        UserEntity user = getUserBySubject(userDetails.getUsername());
        userMapper.updateUserInfo(user, request);

        return userMapper.toUserResponse(user);
    }

    @Transactional
    @Override
    public DeleteResponse deleteUser(UserDetails userDetails) {

        log.info("Delete user, user-email - {}", userDetails.getUsername());
        UserEntity user = getUserBySubject(userDetails.getUsername());
        user.setState(UserState.DELETED);

        return new DeleteResponse();
    }

    @Override
    public void checkExistUser(String email) {

        log.info("Check exist user, user-email - {}", email);
        if (userRepository.existsByEmail(email)) {
            throw new ApplicationServiceException(HttpStatus.BAD_REQUEST, "User with such email already exist");
        }
    }
}
