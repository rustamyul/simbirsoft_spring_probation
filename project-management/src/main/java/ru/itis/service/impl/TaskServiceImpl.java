package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.itis.api.dto.request.TaskRequest;
import ru.itis.api.dto.response.DeleteResponse;
import ru.itis.api.dto.response.PageOfListResponse;
import ru.itis.api.dto.response.TaskResponse;
import ru.itis.api.enums.TaskState;
import ru.itis.exception.ApplicationNotFoundException;
import ru.itis.exception.ApplicationServiceException;
import ru.itis.model.ProjectEntity;
import ru.itis.model.ReleaseEntity;
import ru.itis.model.TaskEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.ProjectRepository;
import ru.itis.repository.ReleaseRepository;
import ru.itis.repository.TaskRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.TaskService;
import ru.itis.utils.mapper.TaskMapper;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final ReleaseRepository releaseRepository;
    private final ProjectRepository projectRepository;

    private final TaskMapper taskMapper;

    @Override
    public TaskResponse getTask(UUID id) {
        log.info("Get task through id, task-id - {}", id);
        return taskMapper.toTaskResponse(
                taskRepository.findById(id)
                        .orElseThrow(() -> new ApplicationNotFoundException("Task not found"))
        );
    }

    @Override
    public PageOfListResponse<TaskResponse> getTasks(int page, int size, Specification<TaskEntity> spec) {

        log.info("Get list of tasks with pagination, page - {}, size - {}", page, size);
        Page<TaskEntity> result = taskRepository.findAll(spec, PageRequest.of(page, size, Sort.by("createDate").descending()));
        return PageOfListResponse.<TaskResponse>builder()
                .list((List<TaskResponse>) taskMapper.toTaskResponseList(result.getContent()))
                .size(size)
                .totalPage(result.getTotalPages())
                .build();
    }

    @Transactional
    @Override
    public TaskResponse createTask(UserDetails userDetails, TaskRequest request) {

        log.info("User with email - {} create task, executor-id - {}, project-id - {}, release - {}",
                userDetails.getUsername(), request.getExecutorId(), request.getProjectId(), request.getReleaseId());
        UserEntity author = userRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(() -> new ApplicationNotFoundException("User(author) not found"));
        UserEntity executor = userRepository.findById(request.getExecutorId())
                .orElseThrow(() -> new ApplicationNotFoundException("User(executor) not found"));
        ProjectEntity project = projectRepository.findById(request.getProjectId())
                .orElseThrow(() -> new ApplicationNotFoundException("Project not found"));
        ReleaseEntity release = releaseRepository.findById(request.getReleaseId())
                .orElseThrow(() -> new ApplicationNotFoundException("Release not found"));

        TaskEntity newTask = taskMapper.toTaskEntity(request);
        newTask.setAuthor(author);
        newTask.setExecutor(executor);
        newTask.setProject(project);
        newTask.setRelease(release);
        newTask.setState(TaskState.IN_PROGRESS);
        return taskMapper.toTaskResponse(
                taskRepository.save(newTask));
    }

    @Transactional
    @Override
    public TaskResponse updateTask(UserDetails userDetails, UUID id, TaskRequest request) {

        log.info("User with email - {} update task, task-id - {}", userDetails.getUsername(), id);
        TaskEntity task = taskRepository.findById(id)
                .orElseThrow(() -> new ApplicationNotFoundException("Task not found"));

        if (!task.getAuthor().getEmail().equals(userDetails.getUsername())) {
            throw new ApplicationServiceException(HttpStatus.FORBIDDEN, "Access to the task is denied to the user");
        }
        taskMapper.updateTask(task, request);
        return taskMapper.toTaskResponse(task);
    }

    @Transactional
    @Override
    public DeleteResponse deleteTask(UserDetails userDetails, UUID id) {

        log.info("User with email - {} delete task, task-id - {}", userDetails.getUsername(), id);
        TaskEntity task = taskRepository.findById(id)
                .orElseThrow(() -> new ApplicationNotFoundException("Task not found"));

        if (!task.getAuthor().getEmail().equals(userDetails.getUsername())) {
            throw new ApplicationServiceException(HttpStatus.FORBIDDEN, "Access to the task is denied to the user");
        }
        taskRepository.delete(task);
        return new DeleteResponse();
    }

    @Override
    public TaskResponse changeTaskState(UserDetails userDetails, UUID id, TaskState state) {

        String username = userDetails.getUsername();
        log.info("User with email - {} change task state, task-id - {}", username, id);
        TaskEntity task = taskRepository.findById(id)
                .orElseThrow(() -> new ApplicationNotFoundException("Task not found"));

        if (!task.getAuthor().getEmail().equals(username) ||
                !task.getExecutor().getEmail().equals(username)) {
            throw new ApplicationServiceException(HttpStatus.FORBIDDEN, "Access to the task is denied to the user");
        }
        task.setState(state);
        return taskMapper.toTaskResponse(task);
    }
}
