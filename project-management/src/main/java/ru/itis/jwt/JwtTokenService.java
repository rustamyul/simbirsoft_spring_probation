package ru.itis.jwt;

import ru.itis.api.dto.response.TokenCoupleResponse;
import ru.itis.model.UserEntity;

/**
 * Сервис для работы JSON Web Token.
 */
public interface JwtTokenService {

    /**
     * Метод, предназначенный для проверки токена на валидность.
     *
     * @param token - Access token.
     * @return true - если токен валидный, false - если нет.
     */
    boolean tokenIsValid(String token);

    /**
     * Метод, предназначенный для получения пользователя через токен.
     *
     * @param token - Access token.
     * @return Сущность UserEntity.
     */
    UserEntity getUserByToken(String token);

    /**
     * Метод, предназначенный для генерации Access и Refresh токенов.
     *
     * @param user - Пользователь.
     * @return Пара токенов.
     */
    TokenCoupleResponse generateTokenCouple(UserEntity user);

    /**
     * Метод, предназначенный для обновления Access и Refresh токенов.
     *
     * @param tokenCoupleResponse - Access и Refresh токены.
     * @return Пара токенов.
     */
    TokenCoupleResponse refreshAccessToken(TokenCoupleResponse tokenCoupleResponse);
}
