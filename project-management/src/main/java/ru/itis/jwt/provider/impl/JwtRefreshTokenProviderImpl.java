package ru.itis.jwt.provider.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.itis.exception.TokenRefreshException;
import ru.itis.jwt.provider.JwtRefreshTokenProvider;
import ru.itis.model.AccountRefreshTokenEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.AccountRefreshTokenRepository;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Supplier;

@Component
@RequiredArgsConstructor
public class JwtRefreshTokenProviderImpl implements JwtRefreshTokenProvider {

    private final AccountRefreshTokenRepository accountRefreshTokenRepository;
    @Value("${jwt.expiration.refresh.mills}")
    private Long expirationRefreshInMills;

    @Transactional
    @Override
    public UUID generateRefreshToken(UserEntity user) {
        System.out.println(Instant.now().plusMillis(expirationRefreshInMills));
        AccountRefreshTokenEntity refreshToken = AccountRefreshTokenEntity.builder()
                .account(user)
                .expiryDate(Instant.now().plusMillis(expirationRefreshInMills))
                .build();
        accountRefreshTokenRepository.save(refreshToken);
        return refreshToken.getId();
    }

    @Override
    public AccountRefreshTokenEntity verifyRefreshTokenExpiration(UUID refreshToken) {

        AccountRefreshTokenEntity token = accountRefreshTokenRepository.findById(refreshToken)
                .orElseThrow((Supplier<RuntimeException>) ()
                        -> new TokenRefreshException(refreshToken.toString(), "Токен не существует."));

        accountRefreshTokenRepository.delete(token);

        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            throw new TokenRefreshException(String.valueOf(token.getId()), "Срок действия токена обновления истек.");
        }

        return accountRefreshTokenRepository.save(
                AccountRefreshTokenEntity.builder()
                        .expiryDate(Instant.now().plusMillis(expirationRefreshInMills))
                        .account(token.getAccount())
                        .build());
    }
}
