package ru.itis.jwt.provider;

import ru.itis.model.UserEntity;

import java.util.Date;
import java.util.Map;

public interface JwtAccessTokenProvider {

    String generateAccessToken(String subject, Map<String, Object> data);

    boolean validateAccessToken(String token);

    UserEntity getUserByToken(String token);

    String getRoleFromAccessToken(String token);

    Date getExpirationDateFromAccessToken(String token);

    String getSubjectFromAccessToken(String token);
}
